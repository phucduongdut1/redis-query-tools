import redis
from datetime import datetime

host = '127.0.0.1'
port = '6379'

host_server = 'baccarat-stg-redis.v0hfhw.0001.apne1.cache.amazonaws.com'



r_1 = redis.Redis(host=host_server,
                  port=port,
                  db=1,
                  decode_responses=True)

r_15 = redis.Redis(host=host,
                     port=port,
                     db=15,
                   decode_responses=True)

r_14 = redis.Redis(host=host,
                     port=port,
                     db=14,
                   decode_responses=True)

r_13 = redis.Redis(host=host,
                   port=port,
                   db=13,
                   decode_responses=True)


def change_table_subkey(subkey, num_table):
    subkey = subkey.split('_')
    subkey[1] = str(num_table)
    subkey = '_'.join(subkey)
    return subkey


def check_four_error(r_root, keys_hash):
    check = 0
    for key_hash in keys_hash:
        for hash_key in r_root.scan_iter(key_hash):
            print('check : ', hash_key)
            if 'percent' in hash_key:
                continue
            get_root_hash = r_root.hgetall(hash_key)
            for key in get_root_hash.keys():
                if len(get_root_hash.get(key)) < 5:
                    print('{hash_key} : {key} : {get_root_hash.get(key)} have len < 5')
                    check = 1

    print('check have < 5: ', check)
    print('finished')




keys_hash = ['table_*_*s_21*']


check_four_error(r_1, keys_hash)