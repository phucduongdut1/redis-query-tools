import redis
from datetime import datetime



host = '127.0.0.1'
port = '6379'
db = 0

# r_0 = redis.Redis(host=host,
#                   port=port,
#                   db=0,
#                   decode_responses=True)

r_1 = redis.Redis(host=host,
                  port=port,
                  db=1,
                  decode_responses=True)

r_15 = redis.Redis(host=host,
                     port=port,
                     db=15,
                   decode_responses=True)

r_14 = redis.Redis(host=host,
                     port=port,
                     db=14,
                   decode_responses=True)

r_13 = redis.Redis(host=host,
                   port=port,
                   db=13,
                   decode_responses=True)


def change_table_subkey(subkey, num_table):
    subkey = subkey.split('_')
    subkey[1] = str(num_table)
    subkey = '_'.join(subkey)
    return subkey


def send_hash_to_another_db(r_root, r_child, keys_hash, start_new_table, number_of_table):
    for key_hash in keys_hash:
        start = datetime.now()
        num_part = 100000
        for subkey in r_root.scan_iter('*' + key_hash):
            get_root_hash = r_root.hgetall(subkey)
            for i in range(0, len(get_root_hash), num_part):
                subdict = dict(list(get_root_hash.items())[i:i + num_part])

                for number_table in range(0, number_of_table):
                    subkey = change_table_subkey(subkey, start_new_table + number_table)
                    r_child.hmset(subkey, subdict)
            print('[INFO] ' + subkey + ' transferred with time: ' + str(datetime.now() - start))


def push_new_charts_for_new_tables(app_config, start_new_table, number_of_table):
    candle_chart_table_1_link = app_config.get('FOLDER_CHART')\
        .format(table=1) + '/' + app_config.get('FILE_NAME_CANDLE_CHART')
    line_chart_table_1_link = app_config.get('FOLDER_CHART')\
        .format(table=1) + '/' + app_config.get('FILE_NAME_LINE_CHART')

    for table in range(start_new_table, number_of_table + 1):
        folder = app_config.get('FOLDER_CHART').format(table=table)
        file_name_candle = folder + '/' + app_config.get('FILE_NAME_CANDLE_CHART')
        file_name_line = folder + '/' + app_config.get('FILE_NAME_LINE_CHART')

        copyfile(candle_chart_table_1_link, file_name_candle)
        copyfile(line_chart_table_1_link, file_name_line)

        upload_to_s3(file_name_candle, file_name_candle)
        upload_to_s3(file_name_line, file_name_line)


keys_hash = ['1*signal*', '1*s_*']


start_new_table = 9
number_of_table = 2
send_hash_to_another_db(r_1, r_14, keys_hash, start_new_table, number_of_table)