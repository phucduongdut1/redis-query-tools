# importing the multiprocessing module
import multiprocessing
import time


def print_cube(num):
    """
    function to print cube of given num
    """
    while True:
        print("p1: " + str(num))
        time.sleep(0.2)


def print_square(num):
    """
    function to print square of given num
    """
    # for i in range(1, 10):
    while True:
        print("p2: " + str(num))
        time.sleep(0.2)


if __name__ == "__main__":
    # creating processes
    p1 = multiprocessing.Process(target=print_square, args=(1, ))
    #p2 = multiprocessing.Process(target=print_cube, args=(0, ))

    # starting process 1
    p1.start()
    print_cube(2)
    # starting process 2
    #p2.start()

    # wait until process 1 is finished
    #p1.join()
    # wait until process 2 is finished
    #p2.join()

    # both processes finished
    print("Done!")