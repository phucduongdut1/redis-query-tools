import redis
from datetime import datetime

host = '127.0.0.1'
port = '2397'
db = 0

r_15 = redis.Redis(host=host,
                   port=port,
                   db=15,
                   decode_responses=True)

r_14 = redis.Redis(host=host,
                   port=port,
                   db=14,
                   decode_responses=True)

r_13 = redis.Redis(host=host,
                   port=port,
                   db=13,
                   decode_responses=True)

r_1 = redis.Redis(host=host,
                   port=port,
                   db=1,
                   decode_responses=True)


# key_hash = 's_11' / 's_21'/  'signal_11' / 'signal_21'
def send_hash_to_another_db(r_root, r_child, key_hash):
    start = datetime.now()
    num_part = 100000
    for subkey in r_root.scan_iter('*' + key_hash):
        get_root_hash = r_root.hgetall(subkey)
        for i in range(0, len(get_root_hash), num_part):
            subdict = dict(list(get_root_hash.items())[i:i + num_part])
            r_child.hmset(subkey, subdict)
        print('[INFO] ' + key_hash + ' transferred with time: ' + str(datetime.now() - start))


# send_hash_to_another_db(r_1, r_13, 's_21')


def send_string_to_another_db(r_root, r_child, key_prefix):
    start = datetime.now()
    key_search = '*' + key_prefix + '*'
    for subkey in r_root.scan_iter(key_search):
        value = r_root.get(subkey)
        r_child.set(str(subkey), str(value))
    print('[INFO] ' + key_prefix + ' transferred with time: ' + str(datetime.now() - start))


# send_string_to_another_db(r_1, r_13, 'text')
send_hash_to_another_db(r_1, r_13, 's_21')


