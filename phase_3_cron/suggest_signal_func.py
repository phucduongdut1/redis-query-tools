import numpy as np


# self.text_history = text_history
#         self.p_len = p_len
#         self.s_len = s_len
#         self.s_num = s_num
# generate
def generate_suggestions_from_history(p_len, s_len, text_history, s_num, dtype=np.int32):
    """
    Calculate suggestions from main history using dynamic programming
    :return:
    """
    num_patterns = 2 ** p_len
    # 2-dimension array for suggestions
    sgs = [[] for _ in range(num_patterns)]
    for i in range(p_len, len(text_history) - s_len + 1):
        p_val = int(text_history[i - p_len:i], 2)  # Extract pattern and encode to integer
        sgs[p_val].append(int(text_history[i:i + s_len], 2))  # Append occurences
    for i in range(num_patterns):
        u, c = np.unique(sgs[i], return_counts=True)  # distinct occurrences to get futures
        u = u[np.argsort(c)[::-1][:s_num]]  # sort by counts ("c") & get top 50
        if u.shape[0] < s_num:  # Pad value -1 to have 50-item list
            pad_num = s_num - u.shape[0]
            u = np.pad(u, (0, pad_num), 'constant', constant_values=(-1, -1)).astype(dtype)
        sgs[i] = u
    arr = np.array(sgs, dtype=dtype).reshape(-1, s_num)
    return arr







