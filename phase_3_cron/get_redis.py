import redis
from datetime import datetime
from dotenv import load_dotenv


load_dotenv()

host = '127.0.0.1'
port = '2397'
import os


r_1 = redis.Redis(host=host,
                  port=port,
                  db=1,
                  decode_responses=True)

r_2 = redis.Redis(host=host,
                  port=port,
                  db=2,
                  decode_responses=True)

r_15 = redis.Redis(host=host,
                     port=port,
                     db=15,
                   decode_responses=True)

r_14 = redis.Redis(host=host,
                     port=port,
                     db=14,
                   decode_responses=True)

r_13 = redis.Redis(host=host,
                   port=port,
                   db=13,
                   decode_responses=True)


def get_current_update_block(table_num, redis_instance):
    # get current update block text
    key_search = 'table_ ' + str(table_num) + '_current_block'
    block_current = redis_instance.get(key_search)
    return block_current


def get_text_history_redis(table_num, redis_instance):
    # GET ALL TEXTre
    block_current = get_current_update_block(table_num, redis_instance)
    text_history = ""
    for i in range(0, int(os.getenv("NUM_BLOCK_TEXT")) + 1):
        if i == int(block_current):
            continue
        key_search = 'table_' + str(table_num) + '_text_' + str(i)
        text_history += redis_instance.get(key_search)
    return text_history


get_text_history_redis(1, r_2)
















