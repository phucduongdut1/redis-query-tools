import redis


host = '127.0.0.1'
port = '2397'
import os

r_15 = redis.Redis(host=host,
                  port=port,
                  db=15,
                  decode_responses=True)


def get_five_suggestions_from_dict(r_root):
    '''
    get 5 suggestions from dict
    use for transfer db_root[15] to db_de[2]
    -----
    task:
    read all old suggestions
    pick 5 suggestions dict
    del table_{table}_s_{p_len}_{pattern}
    save 5 suggestions dict
    '''
    list_key_hash = ['s_11', 's_21']
    for key_hash in list_key_hash:
        suggestions_dict = r_root.hgetall(key_hash)
        five_suggestions_result = {}
        for key, suggestions in suggestions_dict.items():
            suggestions = suggestions.split(',')
            five_suggestions = suggestions[:5]
            check_list = []
            for s in five_suggestions:
                temp_sug = "{0:025b}".format(s)
                check_list.append(temp_sug[0])
            if '0' not in check_list:
                for k in range(5, 50):
                    temp_sug = "{0:025b}".format(suggestions[k])
                    if temp_sug[0] == '0':
                        five_suggestions[-1] = suggestions[i][k]
                        five_suggestions_result[app_config.get('REDIS_KEY_SUGGESTION')
                            .format(p_len=p_len, pattern=str(i))] = ','.join(str(x) for x in five_suggestions.tolist())
            elif '1' not in check_list:
                pass
            else:
                pass






get_five_suggestions_from_dict(r_15)