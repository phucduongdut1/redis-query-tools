import redis
from datetime import datetime



host = '127.0.0.1'
port = '2397'
db = 0

# r_0 = redis.Redis(host=host,
#                   port=port,
#                   db=0,
#                   decode_responses=True)

# r_1 = redis.Redis(host=host,
#                   port=port,
#                   db=1,
#                   decode_responses=True)

r_15 = redis.Redis(host=host,
                     port=port,
                     db=15,
                   decode_responses=True)

r_14 = redis.Redis(host=host,
                     port=port,
                     db=14,
                   decode_responses=True)

r_13 = redis.Redis(host=host,
                   port=port,
                   db=13,
                   decode_responses=True)


def transfer_list_to_hash(r_root, r_child, key_hash_list):
    for key_prefix in key_hash_list:
        key_search = key_prefix + '*'
        for subkey in r_root.scan_iter(key_search):
            # delete the key
            #print(subkey)
            value = r_root.lrange(subkey, 0, -1)
            value = [str(v) for v in value]
            r_child.hset(key_prefix, subkey, ','.join(value))
            print('add key: ' + str(subkey) + ' value: ' + ','.join(value))
        print('[INFO] ' + key_search + 'saved')


# use when change signal to hash
def transfer_string_to_hash(r_root, r_child, key_hash_list):
    for key_prefix in key_hash_list:
        key_search = key_prefix + '*'
        for subkey in r_root.scan_iter(key_search):
            value = r_root.get(subkey)
            r_child.hset(key_prefix, subkey, value)
        print('[INFO] ' + key_prefix + 'transferred')


# use when transfer db to another db
def transfer_string_to_string(r_root, r_child, r_child2, key_text_list, tables=None):
    for key_prefix in key_text_list:
        key_search = key_prefix + '*'
        for subkey in r_root.scan_iter(key_search):
            value = r_root.get(subkey)
            if tables != None:
                for table in tables:
                    subkey_temp = table + '_' + subkey
                    r_child.set(str(subkey_temp), str(value))
                    r_child2.set(str(subkey_temp), str(value))
        print('[INFO] ' + key_prefix + 'transferred')


def transfer_list_to_list(r_root, r_child, r_child2, key_text_list, tables=None):
    for key_prefix in key_text_list:
        key_search = key_prefix + '*'
        for subkey in r_root.scan_iter(key_search):
            value = r_root.lrange(subkey, 0, -1)
            if tables != None:
                for table in tables:
                    subkey_temp = table + '_' + subkey
                    r_child.rpush(subkey_temp, *value)
                    r_child2.rpush(subkey_temp, *value)
        print('[INFO] ' + key_prefix + 'transferred')


def transfer_list_to_list_massive(r_root, r_child, r_child2, key_text_list, tables=None):
    for key_prefix in key_text_list:
        key_search = key_prefix + '*'
        for subkey in r_root.scan_iter(key_search):
            value = r_root.lrange(subkey, 0, -1)
            if tables != None:
                for table in tables:
                    subkey_temp = table + '_' + subkey
                    r_child.rpush(subkey_temp, *value)
                    r_child2.rpush(subkey_temp, *value)
        print('[INFO] ' + key_prefix + 'transferred')



def retrieve_hash_key(key_hash_list, redis_instance):
    for key_hash in key_hash_list:
        get_hash = redis_instance.hgetall(key_hash)
        print(get_hash)
        # return get_hash


def transfer_hash_to_hash(r_root, r_child, r_child2, key_text_list, tables=None):
    start = datetime.now()
    print('start time hash_to_hash: ', str(start))
    keys_hash = []
    num_part = 100000
    for key_hash in key_text_list:
        get_root_hash = r_root.hgetall(key_hash)
        if tables != None:
            for table in tables:
                keys_hash.append(table + '_' + key_hash)
        for i in range(0, len(get_root_hash), num_part):
            subdict = dict(list(get_root_hash.items())[i:i + num_part])
            for key_table in keys_hash:
                r_child.hmset(key_table, subdict)
                r_child2.hmset(key_table, subdict)
    print('[INFO] ' + key_hash + 'transferred with time: ' + str(datetime.now() - start))


transfer_hash_to_hash



# ['line', 'text', 'candle', 's_11', 's_21', 'signal_21', 'signal_11']
def transfer_data_and_hash_suggestions_signals(r_root, r_child, key_list):
    if 'text' in key_list:
        transfer_string_to_string(r_root, r_child, ['text'])

    if 'line' in key_list:
        transfer_list_to_list(r_root, r_child, ['line'])

    if 'candle' in key_list:
        transfer_list_to_list(r_root, r_child, ['candle'])

    if 's_11' in key_list:
        transfer_list_to_hash(r_root, r_child, ['s_11'])

    if 's_21' in key_list:
        transfer_list_to_hash(r_root, r_child, ['s_21'])

    if 'signal_21' in key_list:
        transfer_string_to_hash(r_root, r_child, ['signal_21'])

    if 'signal_11' in key_list:
        transfer_string_to_hash(r_root, r_child, ['signal_11'])


# from table root db[15] you can generate tables for another db
# table_num = 8
def generate_data_tables_from_root_table(r_root, r_de, r_be, table_num):
    # key_list = ['text', 'line', 'candle', 's_11', 's_21', 'signal_21', 'signal_11']
    key_list = ['line']

    tables = []
    for i in range(1, table_num + 1):
        tables.append('table_' + str(i))

    if 'text' in key_list:
        transfer_string_to_string(r_root, r_de, r_be, ['text'], tables)

    if 'line' in key_list:
        transfer_list_to_list(r_root, r_de, r_be, ['line'], tables)

    if 'candle' in key_list:
        transfer_list_to_list(r_root, r_de, r_be, ['candle'], tables)

    if 's_11' in key_list:
        transfer_hash_to_hash(r_root, r_de, r_be, ['s_11'], tables)

    if 's_21' in key_list:
        transfer_hash_to_hash(r_root, r_de, r_be, ['s_21'], tables)

    if 'signal_21' in key_list:
        transfer_hash_to_hash(r_root, r_de, r_be, ['signal_21'], tables)

    if 'signal_11' in key_list:
        transfer_hash_to_hash(r_root, r_de, r_be, ['signal_11'], tables)

    print('[INFO] generate data for DE and BE database done ')


generate_data_tables_from_root_table(r_15, r_14, r_13, 2)















